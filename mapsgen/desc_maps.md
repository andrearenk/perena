# Data description in maps

This document presents different maps based on the data used in the ICOPE project. The main objectives are 1) check the correspondance between IIASA maps and PODES at the village level, 2) better understand our SUSENAS consumption sample.

<!-- MarkdownTOC -->

- [IIASA oil palm coverage and comparison with PODES](#iiasa-oil-palm-coverage-and-comparison-with-podes)
	- [IIASA & PODES, desa level](#iiasa--podes-desa-level)
	- [Oil palm coverage at the kecamatan level](#oil-palm-coverage-at-the-kecamatan-level)
- [SUSENAS consumption sample](#susenas-consumption-sample)

<!-- /MarkdownTOC -->


<a id="iiasa-oil-palm-coverage-and-comparison-with-podes"></a>
### IIASA oil palm coverage and comparison with PODES

In this section we present both the administrative entities' share that is considered as oil palm in the IIASA dataset and whenever oil palm is mentioned in the PODES data. Comparing oil palm coverage at the desa and kecamatan level is important for the definition of the treatment variable. Further details on IIASA maps can be displayed by clicking on the ▶ line. <br/>
_Note: currently, treatment is a binary variable taking the value 1 if **any** area in the **village** is palm oil, based on the IIASA dataset_ 

<a id="iiasa--podes-desa-level"></a>
#### IIASA & PODES, desa level

<img src="iiasa/des2000_prop2000_0km.png" width="800">

<img src="podes/oilpalm_mentioned_2003.png" width="800">

<img src="podes/oilpalm_mentioned_2006.png" width="800">

<img src="iiasa/des2000_prop2005_0km.png" width="800">

<img src="podes/oilpalm_mentioned_2008.png" width="800">


<details>
<summary>PODES oil palm definitions</summary>

In 2003, they ask the area and production of 5 (main?) types of plantation in the village. 
In 2006, they ask one signature product. In 2008, they ask which commodity/sub-sector is the main source of income of the majority of the population. 
<br><br/>

</details>



<details>
<summary>IIASA, other years</summary>	

<img src="iiasa/des2000_prop1995_0km.png" width="800">


<img src="iiasa/des2000_prop2010_0km.png" width="800">

</details>


<a id="oil-palm-coverage-at-the-kecamatan-level"></a>
#### Oil palm coverage at the kecamatan level

<img src="iiasa/kec93_prop1995_0km.png" width="800">

<img src="iiasa/kec93_prop2005_0km.png" width="800">


<details>
<summary>Other years</summary>	

<img src="iiasa/kec93_prop2000_0km.png" width="800">

<img src="iiasa/kec93_prop2010_0km.png" width="800">

<img src="iiasa/kec93_prop2015_0km.png" width="800">

</details>



<a id="susenas-consumption-sample"></a>
### SUSENAS consumption sample

The rural sample at the 2 time periods selected for the ICOPE analysis are shown directly below, and further information can be found by clicking on ▶. <br/>
:warning: The treated group are the villages that have a strictly positive share of their area within 15km of a IIASA plantation in 2005 but not in 1995. The control group are all the villages that never had any plantation, provided their suitability index is high enough.


<img src="susenas/sampleIcope_1996_rural.png" width="800">

<img src="susenas/sampleIcope_2005_rural.png" width="800">


<details>
<summary>Other rural samples</summary>	

<img src="susenas/sampleIcope_1999_rural.png" width="800">

<img src="susenas/sampleIcope_2011_rural.png" width="800">

</details>


<details>
<summary>Non-rural samples</summary>	

<img src="susenas/sampleIcope_1996.png" width="800">

<img src="susenas/sampleIcope_1999.png" width="800">

<img src="susenas/sampleIcope_2005.png" width="800">

<img src="susenas/sampleIcope_2011.png" width="800">

</details>