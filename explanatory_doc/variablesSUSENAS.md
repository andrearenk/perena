
## SUSENAS: information on financial transactions 

#### SUSENAS 1993

Module questionnaire (**max: 57,537 hh**) > V. Income from plantation crops, including cost of production >  F. Other receipts and disbursement during last month and year: <br/>
*susenas93_561, individual level* <br/>
- receipts > borrowing/loan repayment 6 (9,352 occ.)<br/>
- receipts > borrowing by mortgage 7 (731 occ.)<br/>
susenas93_562, individual level* <br/>
- disbursements > lending/loan repayment 5 (12,464!=0) <br/>
- disbursements > deposit 4 (9,259!=0)<br/>

<details>
<summary>notes on variables</summary>    
In susenas93_561: b5fk1 from 1 to 9 and 99: probably gives category of revenue, and then in same row b5fk2 gives value last 12 months and b5fk3 value previous month. <br/>
In susenas93_562: k5f2k21/9= seems there is only the last 12 months value
</details>

#### SUSENAS 1996

Module questionnaire (**max: 60,374 hh**)> IV. Income, revenue and expenses for non food category during the past 12 months > 6. Other expenses and revenue > <br/>
- Expenses b: Others (saving, paid of insurance, loans/paid back loans, down payment, saving club payment) <br/>
- Revenue b: Others (withdrew savings, claimed other insurance, received saving club payment, received loans, panwed goods, etc.) <br/>

#### SUSENAS 1999

Ssn99ssn_h (**204,531 hh**): Has this hh possessed/purchased/sold or pawned land in the last 6 months? (b5r18j1/2/3)

Ssn99m_5 (**61,483 hh**): V. Income, revenue and expenditure of non-consumption during 12 months ago > 6. Other expenses and revenue > <br/>
- Expenses > b. Others (saving, insurance premium, lend/paid of owe, down payment, saving club contribution, etc) <br/>
- Revenue > b. Others (saving, claimed life insurance, saving club gain,
borrowing, pawning, etc) <br/>
**no clear variable names from document (closest p42) ie no codebook for these specific variables**

#### SUSENAS 2002

Ssn02m_inc (**64,422 hh**): <br/>
- b5fr08k2: transfer revenue received in the last 12 months - others (production equipment and other capital goods) <br/>
- b5fr06k4: transfer spending paid in the last 12 months - others (production equipment and other capital goods) <br/>
- b5gr01k2: Revenue through financial transaction in the last one year - saving withdrawal <br/>
- b5gr06k2: Revenue through financial transaction in the last one year - borrow money <br/>
- b5gr01k4: Spending through financial transaction in the last one year - saving <br/>
- b5gr02k4: Spending through financial transaction in the last one year - paying debt <br/>

#### SUSENAS 2005

Ssn05m_51DG (**63,610 hh**): <br/>
- b51k2g1: financial transaction -revenue- in the last 1 year - saving withdrawal <br/>
- b51k2g6: financial transaction -revenue- in the last 1 year - borrow money <br/>
- b51k2g31: financial transaction -spending- in the last 1 year - saving <br/>
- b51k2g32: financial transaction -spending- in the last 1 year - paying debt <br/>

#### SUSENAS 2008

Ssn08k_h (VII. Other social economy characteristics) (**282,387 hh**): <br/>
- b7r3a?: Any househld member receiving business credit through <br/>
    + kecamatan development project 1 <br/>
    + urban development project 2 <br/>
    + other government program 3 <br/>
    + bank program 4 <br/>
    + cooperative/foundation program 5 <br/>
    + private individual 6 <br/>
    + others 7 <br/>

Module questionnaire > V. Income, earning and non-consumption expenditure > D. Transfer revenue and spending and financial transaction in the last 12 months >  <br/>
- Revenue > 2. Revenue and financial transaction (saving withdrawal, account receivable payment, life/pension/education insurance claim, saving clubs, borrowing, pawning goods) <br/>
- Spending > 2.Expenditure financial transaction (saving, account payable payment, life/pension/education insurance premium, saving club payment, lending money, reclaiming pawned good, etc.) <br/>
**No codebook for module dataset + "data from block V of income questionnaire are not available on dataset held by ANU"**

#### SUSENAS 2011

Core questionnaire (**285,303 hh**)> VII.A. Poverty eradication program > 
Question 3.a: has the household ever received a business credit in the past one year? > <br/>
- Public empowerment National Program 1 <br/>
- Other government program 2 <br/>
- Public Business credit (KUR) 3 <br/>
- Bank program other than KUR 4 <br/>
- Cooperatives program 5 <br/>
- Individual 6 <br/>
- Other 7 <br/>

Module questionnnaire > V. Income, earning and non-consumption expenditure > D. Transfer revenue and expenditure and financial transactions during the last 3 months > <br/>
- Revenues > 2. Revenues from financial transactions (making savings, return on receivables, claims of insurance/pension/education, get a social gathering, borrow money, return on accounts receivable, pledge of goods) <br/>
- Expenditures > 2. Expenses from financial transactions (savings,pay off debt, premiums of life insurance/pension/education, paying social gathering, lend money, paying accounts payable, redeem the pledge of goods) 


## SUSENAS: information on house status

<details>
<summary>more</summary>

*SUSENAS 1993*: Module questionnaire > IV.2. > (210) Status of dwelling unit: own, contract, rent, lease purchase, official rent, others <br/>

*SUSENAS 1996*: Core questionnaire > III.2.? > (233) House living in is : owned property/free of rent, lease, rent, installment payment, official house, other <br/>

*SUSENAS 1999*: Core questionnaire (Ssn99k_h) > Ownership status of the house (k8r1): privatly own, leased, rented, rent/lease free, provided by government/company, own by parents/relative, other <br/>

*SUSENAS 2002*: Core questionnaire (Ssn02k_h) > Ownership status of the house (b6r1): privatly own, leased, rented, rent/lease free, provided by government/company, own by parents/relative, other <br/>

*SUSENAS 2005*: Core questionnaire (Ssn05k_h) > Ownership status of the house (b6r1): privatly own, lease, rent, rentor lease free, provided by government/company, own by parents/relative, other <br/>

*SUSENAS 2008*: Core questionnaire (Ssn08k_h) > Ownership status of the house (b6r1): privatly own, lease, rent, rentor lease free, provided by government/company, own by parents/relative, other <br/>

*SUSENAS 2011*: Core questionnaire VI. > 3. Residential building status: owned, lease, rent, free lease owned by other people, free lease owned by parents/family, company house, other
</details>


