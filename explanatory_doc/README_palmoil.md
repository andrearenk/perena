# README_palmoil

<!-- MarkdownTOC -->

- ["Palm oil treatment" definition](#palm-oil-treatment-definition)
    - [Intersection between \(buffer around\) industrial plantations and administrative areas](#intersection-between-buffer-around-industrial-plantations-and-administrative-areas)
        - [Administrative level: desa \(village\) or kecamatan \(sub-district\)](#administrative-level-desa-village-or-kecamatan-sub-district)
        - [Buffer around plantation](#buffer-around-plantation)
        - [Threshold proportion](#threshold-proportion)
        - [Code](#code)
    - [Number of treated kecamatans with different thresholds](#number-of-treated-kecamatans-with-different-thresholds)
- [Palm oil treatment and households in SUSENAS](#palm-oil-treatment-and-households-in-susenas)
    - [Recap: # observations after matching with map kec93](#recap--observations-after-matching-with-map-kec93)
    - [Number of SUSENAS observations by treatment kec+10km+0%](#number-of-susenas-observations-by-treatment-kec10km0)

<!-- /MarkdownTOC -->

### "Palm oil treatment" definition

The aim of this project is to establish the impact of palm oil development on household characteristics (food security, debt, household composition and others) in Sumatra and Kalimantan, which are the largest producing regions in Indonesia. 
In this project, palm oil development is captured by the development of large-scale plantations at 5 points in time between 1995 and 2015 using remote sensing imagery (source: [K.G. Austin et al., 2017](http://pure.iiasa.ac.at/id/eprint/14817/1/1-s2.0-S0264837717301552-main.pdf)). 

<details>
<summary>More details on methodology</summary>   
The maps for large-scale plantations were created by hand, from visual interpretation of Landsat imagery. The resolution of these maps is 250x250m. Large-scale plantations were spotted based on their rectilinear patterns and context indicators such as road networks, mill facilities and management buildings. Recently cleared areas adjacent to existing plantations also following a grid formation were also included as part of the large scale plantation. The supplementary appendix of the paper contains a comparison of estimates of oil palm plantation area with other studies (Figure 2) and the confusion matrices (Table 3).  
</details>

#### Intersection between (buffer around) industrial plantations and administrative areas

To define who is exposed to large-scale palm oil plantation based on administrative areas, some decisions need to be made as there is no "treatment" per say. For instance: until which distance do we consider an area could be impacted by palm oil production? is it about distance or about coverage? which administrative-level is the most adequate?

##### Administrative level: desa (village) or kecamatan (sub-district)

<details>
<summary>Details</summary>
We recover higher information at the kecamatan level because we are able to better track changes in kecamatans compared to desa (cf README_crosswalk), but the village level could add more variation and allow for more precision. Another element worth thinking upon is "uniformity" of treatment measure: very large variations in superficy may introduce non-desirable measurement error. Finally, depending on the identification strategy used, it is worth noting that we follow much more kecamatans across time than villages (cf next detail section)<br/>
One possibility, to combine as much information as we can, would be to have a measure based at the kecamatan level but including also some village information when possible (for instance the centroid coordinates of the village when available). 
<br/><br/>
</details>

<details>
<summary>Frequency kecamatan 93 in SUSENAS across rounds </summary>

These results are based using the relation between the crosswalk and SUSENAS, not using the final result of the match; it is thus an upper bound. <br/> 

<img src="markdownim/freqkec93.png" width="400">

<details>
<summary>More details on frequency and rounds for Sumatra and Kalimantan</summary>

```
     Freq. |                                    round
      keca |      1993       1996       1999       2002       2005       2008       2011 |     Total
    -----------+-----------------------------------------------------------------------------+----------
         1 |         0          0          0          0          0          0          5 |         5 
         2 |         1          0          0          0          1         57         57 |       116 
         3 |        10          5          5          4          9         17         16 |        66 
         4 |        18          9          6         24         35         45         43 |       180 
         5 |       111         99        100         34         52        126        123 |       645 
         6 |        83        153        142        113        144        141        148 |       924 
         7 |       793        793        793        793        793        793        793 |     5,551 
    -----------+-----------------------------------------------------------------------------+----------
     Total |     1,016      1,059      1,046        968      1,034      1,179      1,185 |     7,487 
```
</details>
</details>

<details>
<summary>Frequency desa 2000 in SUSENAS across rounds [NOT FINAL] </summary>

The figures presented in the table below are not final, because consistency of administrative units at the desa level is not finalised: for now everything is set on 2000 administrative division, which implies that 2 villages in 2000 may be only one in 1996 for example. In addition, these results are based using the relation between the crosswalk and SUSENAS, not using the final result of the match; it is thus an upper bound. <br/>

<img src="markdownim/freqdes00.png" width="400">

</details>

##### Buffer around plantation

<details>
<summary>Details</summary>
We consider palm oil plantations are likely to impact households living not only "among" plantations, but maybe also within a certain distance. Either because workers in these plantations may live within a daily-coverable distance or because small-scale plantations (smallholders) are known to often be developed close to industrial plantations, and it may be relevant to consider them as well as part of the story on palm oil production. <br/>
For now, all variables are constructed with 3 different buffer values: 0km, 10km and 20km. 
</details>

##### Threshold proportion

<details>
<summary>Details</summary>
Given we are working with administrative areas (and not household GPS coordinates for example), another important aspect is: once the buffer has been set, when are we starting to consider the administrative area treated? is it whenever the administrative area is in contact with the buffer, or do we want to set a minimum area/proportion in kecamatan covered by the buffer? <br/>
For now, we consider that any contact (ie area/proportion>0) implies treatment. *It can be modified in the code by changing parameter j when building each period apparition of plantation*. 
</details>

##### Code

<details>
<summary>Code (python): merge map and IIASA (includes buffer-construction)</summary> 

```python
path_iiasa = main + 'Indonesie/GIS albers/102.IIASA_indo_oilpalm_map/'
path_desa = main + 'Indonesie/GIS albers/112.map1993from2000/kec93.shp'

desa = gpd.read_file(path_desa)

desa = desa[['id93_kec','geometry']]
desa['island'] = desa['id93_kec'].astype(str).str[0]
desa = desa[(desa.island == '1')|(desa.island == '6')]

years = [1995, 2000, 2005, 2010, 2015]
buffers = [0, 10, 20]

desa['area_m'] = desa.area

for year in years:
    iiasa = gpd.read_file(path_iiasa + 'IIASA_'+str(year)+'.shp')
    
    # construct different buffer sizes (in km)
    for buf in buffers:
        iiasabuf = iiasa.geometry.buffer(buf*1000)
        buffer = gpd.GeoDataFrame({'geometry': iiasabuf})
        # dissolve buffer so that less computation time
        buffer['dissolvefield'] = 1
        buffer = buffer.dissolve(by='dissolvefield')
        
        # intersect between dissolved buffer and desa map + retrieve area of intersection
        intbuf = gpd.overlay(buffer, desa, how='intersection', make_valid=True)
        #intbuf = gpd.overlay(buffer, desa, how='intersection', make_valid=True, use_sindex=None)
        intbuf['int'+str(year)+'_'+str(buf)+'km'] = intbuf.area
        
        # merge dataframes in order to construct one dataframe with all information (desa)
        df = [intbuf['id93_kec'],intbuf['int'+str(year)+'_'+str(buf)+'km']]
        df = pd.concat(df, axis=1)
        desa = pd.merge(desa, df, on=['id93_kec'], how='outer')

# export dataframe to stata
df = pd.DataFrame(desa)
df = pd.DataFrame(df.drop(columns='geometry'))
df.to_stata(out + 'iiasa_sumkali_kec93.dta')
```
</details>

<details>
<summary>Code (Stata): define treatment status</summary> 

Below is a selected part of *IIASA+SUSENAS 1.Kecamatan93.do*, in Indonesie/plantations/CODE/DATA CONSTRUCTION/

```stata

use "$clean/iiasa_sumkali_kec93.dta", clear
lab var area_m "Kecamatan 1993 area, in m2"
"
** no area in overlay = 0 m2 close to plantation
foreach v of varlist int*km{
    replace `v' = 0 if missing(`v') // if no interacted area, 0 m2 overlap
}
drop index

** count if plantation disappears in 2010 // 1 2 0
foreach i in 0 10 20{
    count if int2005_`i'km>int2010_`i'km&int2010_`i'km==0
}

** change names 
foreach v of varlist int*{
    local y = substr("`v'",4,4)
    local buf = substr("`v'",9,.)
    local newname = "areapalm"+"_`buf'"+ "_`y'"
    rename `v' `newname'
    
    lab var `newname' "Palm oil area in `y', `buf' buffer"
}

** construct % palm per village 
foreach i in 0 10 20{
    foreach y of numlist 1995(5)2015{
        g ppalm_`i'km_`y' = areapalm_`i'km_`y'/area_m
        lab var ppalm_`i'km_`y' "Proportion of kecamatan with palm oil (`i'km buffer)"
    }
}

** build each period apparition of plantation
foreach i in 0 10 20{
    local j 0
        g plant_`i'km_95 =  ppalm_`i'km_1995>`j'
        g plant_`i'km_9500 = ppalm_`i'km_1995<=`j'&ppalm_`i'km_2000>`j'
        g plant_`i'km_0005 = ppalm_`i'km_2000<=`j'&ppalm_`i'km_2005>`j'
        g plant_`i'km_0510 = ppalm_`i'km_2005<=`j'&ppalm_`i'km_2010>`j'
        g plant_`i'km_1015 = ppalm_`i'km_2010<=`j'&ppalm_`i'km_2015>`j'
        
        lab var plant_`i'km_95 "Plantation in 1995 (prop=`j'%, `i'km buffer)"
        lab var plant_`i'km_9500 "First plantation between 1995 and 2000 (prop=`j'%,`i'km buffer)"
        lab var plant_`i'km_0005 "First plantation between 2000 and 2005 (prop=`j'%,`i'km buffer)"
        lab var plant_`i'km_0510 "First plantation between 2005 and 2010 (prop=`j'%,`i'km buffer)"
        lab var plant_`i'km_1015 "First plantation between 2010 and 2015 (prop=`j'%,`i'km buffer)"
}
```
</details>

#### Number of treated kecamatans with different thresholds

<img src="markdownim/treatkec_var.png" width="550">


### Palm oil treatment and households in SUSENAS

####Recap: # observations after matching with map kec93

In total, 1,567,965 observations from SUSENAS over a total of 1,653,015 were matched, each round being matched to a consistent administrative entity (kecamatan in 1993 from map2000). More details in README_crosswalk. 

<details>
<summary>Number of matched observations per round for Sumatra and Kalimantan vs. absolute number of obs per round</summary>

```
    | round |   Sumatra Kalimantan |     Total
    --------+----------------------+----------
       1993 |    44,503     17,669 |    62,172 
       1996 |    45,171     17,659 |    62,830 
       1999 |    44,474     17,584 |    62,058 
       2002 |    53,556     22,803 |    76,359 
       2005 |    65,182     30,415 |    95,597 
       2008 |    79,466     30,727 |   110,193 
       2011 |    80,772     28,463 |   109,235 
    --------+----------------------+----------
      Total |   413,124    165,320 |   578,444


    | round |   Sumatra Kalimantan |     Total
    --------+----------------------+----------
       1993 |    46,455     17,749 |    64,204 
       1996 |    47,043     17,787 |    64,830 
       1999 |    46,682     17,648 |    64,330 
       2002 |    53,684     22,803 |    76,487 
       2005 |    66,761     30,463 |    97,224 
       2008 |    81,312     31,408 |   112,720 
       2011 |    80,931     28,493 |   109,424 
    --------+----------------------+----------
      Total |   422,868    166,351 |   589,219 


```
</details>

#### Number of SUSENAS observations by treatment kec+10km+0%

<img src="markdownim/nbrsusobs1.png" width="600">




