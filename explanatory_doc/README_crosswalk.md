# README_crosswalk

<!-- MarkdownTOC -->

- [Procedure to have consistent location across survey waves](#procedure-to-have-consistent-location-across-survey-waves)
- [Details on crosswalks used](#details-on-crosswalks-used)
    - [Desa level \(*des_crosswalk*\)](#desa-level-des_crosswalk)
    - [Kecamatan level \(*combined_kec*\)](#kecamatan-level-combined_kec)
- [SUSENAS: best year-identifiers in crosswalks](#susenas-best-year-identifiers-in-crosswalks)
    - [Kecamatan level](#kecamatan-level)
    - [Desa level](#desa-level)
- [Adding a map](#adding-a-map)
    - [Chosing between maps with a given crosswalk](#chosing-between-maps-with-a-given-crosswalk)
    - [Consistent geography across time](#consistent-geography-across-time)
- [Final match between SUSENAS and kecamatan map 1993](#final-match-between-susenas-and-kecamatan-map-1993)

<!-- /MarkdownTOC -->


### Procedure to have consistent location across survey waves 

Administrative entities and/or their denomination (identifier) change across time. And to exploit a spatial dimension across different survey rounds, one may need to have consistent administrative entities across time. 

To be able to exploit such a dimension, one thus needs different elements: survey and round-based geographical identifiers, and a crosswalk to be able to know correspondance across identifiers across time. A crosswalk can be either given, or could theoretically be reconstructed from different maps. If some spatial attributes from GIS data need to be matched to the survey data, a map containing identifiers will also be needed. 

The procedure chosen in this project is as follows: <br/>
- for each round of survey, find the year in crosswalk that best fits the survey identifiers <br/>
- find the year in crosswalk that best fits the map/GIS identifiers <br/>
- use the crosswalk to have a year-wise correspondence between GIS and survey identifiers 


### Details on crosswalks used 

#### Desa level (*des_crosswalk*)

The crosswalk used at the desa level is Lisa Cameron's crosswalk. The construction of *des_crosswalk* from the original crosswalk can be found in *Crosswalk 0. Admlevels_LCcrossw.do*.

Lisa Cameron's crosswalk covers the period 1998 to 2011. The original crosswalk from Lisa Cameron (*crosswalk_orig*) encompasses all levels of administrative entities (provinces, kabupaten, kecamatan and desa). The different levels are identified based on the number of 0s: provinces are in form XX00000000, kabupatens XXXX000000, kecamatans XXXXXXX000 and desa XXXXXXXXXX. In order to obtain level-wise crosswalk, relevant entities are selected based on the previous description (if at least one identifiers in the row fills the format). 

#### Kecamatan level (*combined_kec*)

The crosswalk used at the kecamatan level is a combination between Olken's and Lisa Cameron's crosswalk. The construction of this crosswalk can be found in *crosswalk_combined_kec.do*.

Olken crosswalk (obtained from Martin from Basi, at kecamatan level) includes identifiers from the 1970s to 2002. 
Given there is an overlap with Lisa Cameron's crosswalk, it is possible to merge the 2 to cover a longer time period and improve the quality i.e. replace missing observations from one crosswalk with non-missing observations with the other.

The procedure needs to be sequential in order to avoid doing m:m merges i.e. allocate "randomly" some past and future identifiers based on present identifiers. In order to be able to do so, the master used in mergers is always *kec_crosswalk* and using is derived from Olken crosswalk, each time dropping the more advanced in time identifiers in order to have unique values. <br/>
NB: it works because it seems there were only subdivisions of kecamatan. 

The procedure:    
1. merge first on id 1998, 1999, 2000, 2001, 2002 <br/>
2. drop obs non merged from Olken crosswalk <br/>
3. merge on id 1998, 1999, 2000, 2001 <br/>
4. drop obs non merged from Olken crosswalk <br/>
5. merge on id 1998, 1999, 2000 <br/>
6. drop obs non merged from Olken crosswalk <br/>
7. merge on id 1998 and 1999 <br/>
8. drop obs non merged from Olken crosswalk <br/>
9. merge on id 2000 <br/>
10. special merge for province 13 (manual corrections) <br/>
11. append all datasets dropping duplicates for each append <br/>
12. replace missing values for id 1990, 1993, 1997 with 1998 values


### SUSENAS: best year-identifiers in crosswalks

#### Kecamatan level

| Round     | Best year | # matched | # non-m SUSENAS  | # non-m crosswalk | 
| ------------- |-------------:| -----:|-----:| -----:| 
| 1993      | 1993 | 3296 |188 | 517 | 
| 1996    | 1993 | 3473 |175 | 340 | 
| 1999 |  1997 | 3500 | 172 | 340  | 
| 2002 | 2000 | 3381 | 2 | 624 | 
| 2005 | 2006 | 4539 |87 | 444 | 
|2008 | 2007 | 5246 | 173 | 266 | 
|2011 | 2010 | 6097 | 127 | 401 | 

<details>
  <summary>Code (Stata)</summary> 

```stata
* Generate year-wise kecamatan in SUSENAS

use "$susclean/CoreHH.dta", clear 

keep Kecamatan_ID round
duplicates drop

foreach i of numlist 1993(3)2011{
    preserve
    keep if round==`i'
    local newname = substr(string(round), 3, 2)
    rename Kecamatan_ID id`newname'_kec
    g idtest_kec = id`newname'_kec
    drop round 
    duplicates drop 
    save "$crosstemp/susenas/sus_kec`i'.dta", replace
    restore
}

*For each year in SUSENAS, find best year in crosswalk 

foreach j of numlist 1993(3)2011{
    use "$crossin/kec_crosswalk.dta", clear

    global min_nonmatcheduse = 10000
    global min_nonmatchedmast = 100000
    global max_matched = 0

    foreach i in 93 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11{
    quietly{
        preserve
        keep id`i'_des
        duplicates drop
        rename id`i'_kec idtest_kec
        merge 1:1 idtest_kec using "$crosstemp/susenas/sus_kec`j'.dta"
        count if _merge==1
        global nonmatch_frommast = r(N)
        di($nonmatch_frommast)
        count if _merge==2
        global nonmatch_fromuse = r(N)
        di($nonmatch_fromuse)
        count if _merge==3
        global matched = r(N)
        di($matched)
        restore
        if ${nonmatch_frommast}<${min_nonmatchedmast}{
                global min_nonmatchedmast = $nonmatch_frommast
                global year_min = `i'
            }
        if ${nonmatch_fromuse}<${min_nonmatcheduse}{
                global min_nonmatcheduse = $nonmatch_fromuse
                global year_min = `i'
            }
        if ${matched}>${max_matched}{
                global max_matched = $matched
                global year_max = `i'
            }
    }
    }
    noisily : di(`j')
    noisily : di("Year with minimum non-matched from crosswalk :")
    noisily : di("$year_min ,  $min_nonmatchedmast")
    noisily : di("Year with minimum non-matched from susenas :")
    noisily : di("$year_min ,  $min_nonmatcheduse")
    noisily : di("Year with maximum matched :")
    noisily : di("$year_max ,  $max_matched")
}
```
</details>


#### Desa level

| Round     | Best year           | # matched |# non-m SUSENAS  | # non-m crosswalk | 
| ------------- |-------------:| -----:|-----:| -----:| 
| 1993  | 1998 | 8975 | 2208 | 55172 | 
| 1996  | 1998 | 9615 |  1525 | 54532 |
| 1999 |  1998 |9980 | 1151 | 54167  | 
| 2002 | 2000 | 11639 |513 | 54376 | 
| 2005 | 2003 | 13712 |944 | 53147 | 
|2008 | 2006 | 14749 |1455 | 53351 | 
|2011 | 2010 |22956 | 336 | 51007 | 

<details>
  <summary>Code (Stata)</summary> 

```stata
* Generate year-wise desa in SUSENAS

use "$susclean/CoreHH.dta", clear 

keep Kecamatan_ID round
duplicates drop

foreach i of numlist 1993(3)2011{
    preserve
    keep if round==`i'
    local newname = substr(string(round), 3, 2)
    rename Kecamatan_ID id`newname'_kec
    g idtest_kec = id`newname'_kec
    drop round 
    duplicates drop 
    save "$crosstemp/susenas/sus_kec`i'.dta", replace
    restore
}

*For each year in SUSENAS, find best year in crosswalk 

foreach j of numlist 1993(3)2011{
    use "$crossin/kec_crosswalk.dta", clear

    global min_nonmatcheduse = 10000
    global min_nonmatchedmast = 100000
    global max_matched = 0

    foreach i in 98 99 00 01 02 03 04 05 06 07 08 09 10 11{
    quietly{
        preserve
        keep id`i'_des
        duplicates drop
        rename id`i'_des idtest_des
        merge 1:1 idtest_des using "$crosstemp/susenas/sus_kec`j'.dta"
        count if _merge==1
        global nonmatch_frommast = r(N)
        di($nonmatch_frommast)
        count if _merge==2
        global nonmatch_fromuse = r(N)
        di($nonmatch_fromuse)
        count if _merge==3
        global matched = r(N)
        di($matched)
        restore
        if ${nonmatch_frommast}<${min_nonmatchedmast}{
                global min_nonmatchedmast = $nonmatch_frommast
                global year_min = `i'
            }
        if ${nonmatch_fromuse}<${min_nonmatcheduse}{
                global min_nonmatcheduse = $nonmatch_fromuse
                global year_min = `i'
            }
        if ${matched}>${max_matched}{
                global max_matched = $matched
                global year_max = `i'
            }
    }
    }
    noisily : di(`j')
    noisily : di("Year with minimum non-matched from crosswalk :")
    noisily : di("$year_min ,  $min_nonmatchedmast")
    noisily : di("Year with minimum non-matched from susenas :")
    noisily : di("$year_min ,  $min_nonmatcheduse")
    noisily : di("Year with maximum matched :")
    noisily : di("$year_max ,  $max_matched")
}
```
</details>



### Adding a map

To be able to use GIS data, at one point some geographical entities are needed. In other words, first a map is needed, and can later be reshaped in order to have consistent geographical entities across time. To do so, the first step is to find the set of identifiers in the crosswalk that have the best correspondance to the identifiers in the attribute table of the map. 

#### Chosing between maps with a given crosswalk

To be able to define which map to use among the 2 available, we tested how well the 2 could be matched to the crosswalks (desa and keca levels, detailed on section "details on crosswalk used"), and kept the one with the highest correspondance. <br/>
In the end the 2000 map yields the best results at all levels. The comparison is detailed below, along with the code to obtained these figures. <br/>
NB: map 2000 was manually corrected for multiple polygons in different regions of Kalimantan (6404 to 6204). More details below. 

| Map    | Best year | # matched | # non-m map  | # non-m crosswalk | 
| ------------- |-------------:| -----:|-----:| -----:| 
| 2000 - desa     | 2000 | 70294 | 3104 | 2415 | 
| 2003 - desa    | 2002 | 63446 |5726 | 2853 | 
| 2000 - keca |  2000 | 3871 | 16 | 134  | 
| 2003 - keca | 2002 | 4591 | 50 | 287 | 

Related to the results with 2000 map at kecamatan level: among the 134 kecamatans non-matched to crosswalk, only 4 are not in Papua, and only 2 are in Sumatra and none in Kalimantan. From the map, 6 kecamatans from Sumatra are not found in the crosswalk, and 2 in Kalimatan. However, expect for 2 cases (1 in Kalimantan and 1 in island 7), all codes end with "000" or "888" which is suppose to refer to specific types of area such as natural parks, lakes etc. 

<details>
<summary>Manual correction for kabupaten 6204 in map 2000</summary>

Map 2000 was manually corrected for one kabupaten that had multiple polygons in different regions of Kalimantan (6404 to 6204). It was very lilely a typo. First, the multiple polygons were far from each other, making it very unlikely that there was no mistake. In addition, we know from the crosswalk that there 12 kecamatans inside the "true" 6204 kabupaten, vs. only 6 in kabupaten 6404, while we had 6 polygons missing sharing boundaries with one of the set of kecamatans 6404XXX. This lead to a manual change in the map 2000, at all levels. 
<br><br/>

</details>
<details>
<summary>Code (*carto_bestmatchcw*) - to be modified with appropriate map and variables</summary>
```stata
* Generate clean dta file with map identifiers

use _MAP_, clear
keep _IDENTIFIER(S)_
drop if missing(_KEY_IDENTIFIER_)
duplicates drop 
g idtest_kec = _KEY_IDENTIFIER_
save _MAPID_.dta, replace


* Find what year in the cross best match the identifiers from the map

use _CROSSWALK_, clear

global min_nonmatchedcw = 100000
global min_nonmatcheduse = 100000
global max_matched = 0
quietly{
foreach i in 99 00 01 02 03 04 05 06 07 08 09 10 11{
    preserve
    keep id`i'
    duplicates drop
    rename id`i' idtest
    di("`i'")
    merge 1:1 idtest using _MAPID_.dta
    count if _merge==2
    global nonmatch_fromuse = r(N)
    di($nonmatch_fromuse)
    count if _merge==1
    global nonmatch_fromcw = r(N)
    di($nonmatch_fromcw)
    count if _merge==3
    global matched = r(N)
    di($matched)
    restore
    if ${nonmatch_fromcw}<${min_nonmatchedcw}{
        global min_nonmatchedcw = $nonmatch_fromcw
        global year_mincw = `i'
        }
    if ${nonmatch_fromuse}<${min_nonmatcheduse}{
        global min_nonmatcheduse = $nonmatch_fromuse
        global year_minuse = `i'
        }
    if ${matched}>${max_matched}{
        global max_matched = $matched
        global year_max = `i'
        }
}
noisily : di("Year with minimum non-matched from crosswalk :")
noisily : di($year_mincw)
noisily : di($min_nonmatchedcw)
noisily : di("Year with minimum non-matched from use :")
noisily : di($year_minuse)
noisily : di($min_nonmatcheduse)
noisily : di("Year with maximum matched :")
noisily : di($year_max)
noisily : di($max_matched)

}
```

</details>

#### Consistent geography across time 

A map gives a geographical division at one point in time. Yet, if the goal is to have consistent geographical entities across survey waves, this division may not be ideal. 

In Indonesia, a decentralisation process lead to important changes in administrative divisions across time. At the kecamatan level at least, most of these changes were split-ups. Thus, to obtain consistency, it was decided to use the geographical division in the earliest survey wave (1993). To be able to do so, we use the crosswalk to know be able to aggregate geographical entities from the map, using the id93 from the crosswalk. <br/>
The code to produce this 1993 map at the kecamatan level is given below, using geopandas (python package) and the 2000 map.

<details>
<summary>Code (python)</summary>   
``` python
from shapely.geometry import Point
import pandas as pd
import geopandas as gpd

kec2000 = gpd.read_file(path_kec)
crosswalk = pd.read_csv(path_crosswalk)

kec2000 = kec2000.astype({'PKKECODE': 'int64'})
crosswalk = crosswalk.astype({'id00_kec': 'int64'})

newmap = pd.merge(kec2000, crosswalk, right_on=['id00_kec'], left_on=['PKKECODE'], how='inner')
newmap = newmap.dissolve(by='id93_kec')
newmap.plot()

newmap.to_file(outgis+"kec93.shp")

##### Export to .dta
import simpledbf
sourcedbf=outgis+"kec93.dbf"
df=simpledbf.Dbf5(sourcedbf)
dbf=df.to_dataframe()
dbf.to_stata(outgis+"kec93.dta")
```
</details>

### Final match between SUSENAS and kecamatan map 1993 

Once consistent administrative areas have been built, the final step is to merge these areas to SUSENAS survey data. This is done in *susenas_carto_keca*. In short, each round-wise SUSENAS identifiers is match with their best correspondance in the crosswalk, from which the 1993 identifier (same as the map cf previous section) is retrieved. <br/>
NB: there appear to be kecamatans that were parts of several different kecamatans in 1993. When 2 observations have a similar code in later wave, but not in 1993, only one kecamatan in 1993 is kept. 

53,129 observations from SUSENAS (CoreHH) over 1,589,800 (3.2%) are not matched with the crosswalk. 39,728 observations are further lost as a result of the match between the crosswalk and the map. The final number of observations in SUSENAS is provided in the table below, and more details on the previous matches can be found further below by clicking on desired items.

|island93 |      1993    |   1996    |   1999   |    2002      | 2005   |    2008   |    2011 |     Total |
| --------: |---------:| -----:|-----:|-----:| -----:| -----:| -----:| -----:| 
1 |    44,503   |  45,171  |   44,474  |   53,556   |  65,182 |    79,466   |  80,772 |   413,124 
3 |    83,119  |   84,785   |  84,782   |  86,782  |   89,701  |   89,989   |  93,068 |   612,226 
5 |    15,391   |  17,533  |   18,143   |  18,752   |  20,807  |   20,820   |  22,057 |   133,503 
6 |    17,669  |   17,659  |   17,584  |   22,803  |   30,415   |  30,727  |   28,463 |   165,320
7 |    23,288  |   23,856   |  25,337  |   29,120  |   34,956   |  40,361   |  36,971 |   213,889 
8 |     2,159  |    3,117   |   2,301  |    1,024   |   3,899   |   4,422  |    6,512 |    23,434 
Total |   186,384 |   192,041  |  191,974 |   211,845 |   247,646    | 267,905  |  270,170 | 1,567,965


<details>
<summary>Number of SUSENAS observations per island per round </summary>

NB: island 2 was part of Sumatra (island 1). 

```
   island |      1993       1996       1999       2002       2005       2008       2011 |     Total
-----------+-----------------------------------------------------------------------------+----------
         1 |    46,455     47,043     46,682     53,684     63,536     77,705     77,703 |   412,808 
         2 |         0          0          0          0      3,225      3,607      3,228 |    10,060 
         3 |    84,239     84,897     84,878     86,783     90,068     90,930     93,088 |   614,883 
         5 |    21,951     23,117     23,457     18,752     20,887     22,153     22,097 |   152,414 
         6 |    17,749     17,787     17,648     22,803     30,463     31,408     28,493 |   166,351 
         7 |    23,511     24,719     25,449     29,216     38,591     42,629     37,118 |   221,233 
         8 |     8,524      9,034      7,633        832      6,617      6,542      8,839 |    48,021 
         9 |         0          0          0        576      4,519      7,413     14,737 |    27,245 
-----------+-----------------------------------------------------------------------------+----------
     Total |   202,429    206,597    205,747    212,646    257,906    282,387    285,303 | 1,653,015 
```


</details>

<details>
<summary>Match between SUSENAS and crosswalk </summary>

Master : SUSENAS CoreHH <br/>
Using : round-wise best SUSENAS id and id93 
```
 Province  |       
 unique ID | master on    matched |     Total
-----------+----------------------+----------
        11 |     4,991     35,991 |    40,982 
        12 |       652     99,264 |    99,916 
        13 |       112     63,755 |    63,867 
        14 |         0     42,568 |    42,568 
        15 |       526     34,471 |    34,997 
        16 |     1,167     50,734 |    51,901 
        17 |       620     24,785 |    25,405 
        18 |       880     39,797 |    40,677 
        19 |         0     12,495 |    12,495 
        21 |       222      9,838 |    10,060 
        31 |         0     42,429 |    42,429 
        32 |     1,566    143,145 |   144,711 
        33 |       863    176,933 |   177,796 
        34 |        80     24,223 |    24,303 
        35 |        80    204,713 |   204,793 
        36 |         0     20,851 |    20,851 
        51 |       528     38,804 |    39,332 
        52 |       464     34,244 |    34,708 
        53 |     1,302     60,542 |    61,844 
        54 |    16,530          0 |    16,530 
        61 |       460     40,111 |    40,571 
        62 |       109     38,022 |    38,131 
        63 |       254     47,518 |    47,772 
        64 |       208     39,669 |    39,877 
        71 |     1,627     33,927 |    35,554 
        72 |       623     31,311 |    31,934 
        73 |     3,332    101,002 |   104,334 
        74 |     1,347     30,902 |    32,249 
        75 |       415     11,054 |    11,469 
        76 |         0      5,693 |     5,693 
        81 |       671     19,011 |    19,682 
        82 |     6,905     21,434 |    28,339 
        91 |     5,804          0 |     5,804 
        93 |         0        576 |       576 
        94 |       791     20,074 |    20,865 
-----------+----------------------+----------
     Total |    53,129  1,599,886 | 1,653,015 
```

</details>


<details>
<summary>Match between map and crosswalk</summary> 

Master : round-wise best SUSENAS id and id93 <br/>
Using : map kecamatan 1993 identifiers (from map 2000)

```     
    prov93 | master on  using onl    matched |     Total
-----------+---------------------------------+----------
        11 |         0          1        145 |       146 
        12 |         0          1        242 |       243 
        13 |         0          0        103 |       103 
        14 |         1          0         77 |        78 
        15 |         0          0         54 |        54 
        16 |         0          0        101 |       101 
        17 |         0          0         31 |        31 
        18 |         0          0         80 |        80 
        31 |         0          0         43 |        43 
        32 |         0          0        527 |       527 
        33 |         0          0        531 |       531 
        34 |         0          0         73 |        73 
        35 |         1          0        608 |       609 
        51 |         0          0         51 |        51 
        52 |         0          0         59 |        59 
        53 |         0          0        114 |       114 
        61 |         0         16        108 |       124 
        62 |         0          0         82 |        82 
        63 |         0          0        109 |       109 
        64 |         0          0         73 |        73 
        71 |         0          0         85 |        85 
        72 |         0          0         62 |        62 
        73 |         0          0        185 |       185 
        74 |         0          0         63 |        63 
        81 |         0          0         56 |        56 
        82 |       129          0          0 |       129 
-----------+---------------------------------+----------
     Total |       131         18      3,662 |     3,811 
```
</details>

<details>
<summary>Losses between map and match SUSENAS-crosswalk</summary>

Master : match SUSENAS-crosswalk <br/>
Using : both in SUSENAS-crosswalk and in map 1993 (from 2000)

```
    prov93 | master on    matched |     Total
-----------+----------------------+----------
         . |       241          0 |       241 
        11 |         0     35,991 |    35,991 
        12 |         0     99,264 |    99,264 
        13 |         0     63,755 |    63,755 
        14 |       317     52,073 |    52,390 
        15 |         0     34,471 |    34,471 
        16 |         0     63,229 |    63,229 
        17 |         0     24,785 |    24,785 
        18 |         0     39,556 |    39,556 
        31 |         0     42,429 |    42,429 
        32 |         0    163,996 |   163,996 
        33 |         0    176,933 |   176,933 
        34 |         0     24,223 |    24,223 
        35 |        52    204,661 |   204,713 
        51 |         0     38,804 |    38,804 
        52 |         0     34,244 |    34,244 
        53 |         0     60,455 |    60,455 
        61 |         0     40,111 |    40,111 
        62 |         0     38,022 |    38,022 
        63 |         0     47,518 |    47,518 
        64 |         0     39,669 |    39,669 
        71 |         0     44,981 |    44,981 
        72 |         0     31,311 |    31,311 
        73 |         0    106,695 |   106,695 
        74 |         0     30,902 |    30,902 
        81 |         0     29,887 |    29,887 
        82 |    31,208          0 |    31,208 
-----------+----------------------+----------
     Total |    31,818  1,567,965 | 1,599,783  
```

</details>



